package com.rijksmuseum.app

import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.junit.Test

/**
 * Simulates the db - cached and online objects
 * The logic is implemented in [CollectionsViewModel]
 */
class CollectionsViewModelTest {

    private val mDbItems = ArrayList<Int>()
    private var currPage = 1

    @Test
    fun collectionsTest() = runBlocking {
        repeat(10) { test_page() }
        delay(5000) // so that the program runs
    }

    private suspend fun test_page() {
        val cached = getCachedObjects()
        // do not go to API if our cache is enough
        if (cached.size > 10) {
            assert(mDbItems.size == cached.size)
            for ((index, dbItem) in mDbItems.withIndex()) {
                val cacheItem = cached[index]
                assert(cacheItem == dbItem)
            }
            println("cached ok, no online fetch, test OK")
            return
        }

        val online = getOnlineObjects(currPage++)
        val dbTestArr =  cached + online
        assert(mDbItems.size == dbTestArr.size)
        for ((index, dbItem) in mDbItems.withIndex()) {
            val dbTestitem = dbTestArr[index]
            assert(dbTestitem == dbItem)
        }
        println("db consistent, test OK")
    }

    private suspend fun getCachedObjects(): ArrayList<Int> {
        println("getCachedObjects")
        delay((10 * mDbItems.size).toLong()) // simulating db read
        val arr = mDbItems
        return ArrayList(arr)
    }

    private suspend fun getOnlineObjects(page: Int): ArrayList<Int> {
        println("getOnlineObjects online")
        val onlineObjects = arrayListOf(1 * page, 2 * page, 3 * page)
        delay(500) // simulating online
        mDbItems.addAll(onlineObjects)
        println("getOnlineObjects write")
        delay(100) // simulating db write
        return onlineObjects
    }
}