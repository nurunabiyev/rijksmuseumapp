//
// Created by Nuru Nabiyev on 2019-11-25.
//

#include <jni.h>
#include <string.h>

JNIEXPORT jstring JNICALL
Java_com_rijksmuseum_app_utils_JniUtils_getApiKeyEncoded(JNIEnv *env, jobject instance) {
    // in base 64: 0fiuZFh4
    char *key = "MGZpdVpGaDQ=";
    return (*env)->NewStringUTF(env, key);
}