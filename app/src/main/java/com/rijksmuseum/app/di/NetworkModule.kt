package com.rijksmuseum.app.di

import com.rijksmuseum.app.data.api.server.RijksmuseumService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class NetworkModule {

    @Singleton
    @Provides
    fun provideRijksService(): RijksmuseumService {
        return RijksmuseumService.create()
    }
}