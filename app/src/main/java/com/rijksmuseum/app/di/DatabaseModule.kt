package com.rijksmuseum.app.di

import android.content.Context
import com.rijksmuseum.app.data.api.db.RijksDatabase
import com.rijksmuseum.app.data.api.db.dao.ArtObjectsDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class DatabaseModule {

    @Singleton
    @Provides
    fun provideDatabase(@ApplicationContext context: Context): RijksDatabase {
        return RijksDatabase.getInstance(context)
    }

    @Provides
    fun provideArtDao(rijksDatabase: RijksDatabase): ArtObjectsDao {
        return rijksDatabase.artObjectsDao
    }
}