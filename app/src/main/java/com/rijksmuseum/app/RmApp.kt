package com.rijksmuseum.app

import android.app.Application
import android.os.StrictMode
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class RmApp : Application() {
    companion object {
        // app instance
        private lateinit var instance: RmApp
        fun get(): RmApp = instance
    }

    override fun onCreate() {
        super.onCreate()
        instance = this

        if (BuildConfig.DEBUG) {
            enableStrictMode()
        }
    }

    /**
     * Will make app crash in case of the leaks or improper IO usage
     */
    private fun enableStrictMode() {
        StrictMode.setThreadPolicy(
                StrictMode.ThreadPolicy.Builder()
                        .detectDiskReads()
                        .detectDiskWrites()
                        .detectNetwork()
                        .penaltyLog()
//                        .penaltyDeath()
                        .build()
        )
        StrictMode.setVmPolicy(
                StrictMode.VmPolicy.Builder()
                        .detectActivityLeaks()
                        .detectFileUriExposure()
                        .detectLeakedClosableObjects()
                        .detectLeakedRegistrationObjects()
                        .penaltyLog()
//                        .penaltyDeath()
                        .build()
        )
    }
}