package com.rijksmuseum.app.utils

import android.content.ContentValues
import android.graphics.Bitmap
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import androidx.fragment.app.FragmentActivity
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.OutputStream

suspend fun saveImageToGallery(bitmap: Bitmap, activity: FragmentActivity, appName: String): Boolean {
    val cr = activity.contentResolver

    var isSaved = false
    if (Build.VERSION.SDK_INT >= 29) {
        val values: ContentValues = contentValues()
        values.put(MediaStore.Images.Media.RELATIVE_PATH, "Pictures/$appName")
        values.put(MediaStore.Images.Media.IS_PENDING, true)
        val uri = cr.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
        if (uri != null) {
            try {
                saveImageToStream(bitmap, cr.openOutputStream(uri))
                values.put(MediaStore.Images.Media.IS_PENDING, false)
                cr.update(uri, values, null, null)
                isSaved = true
            } catch (e: FileNotFoundException) {
                e.printStackTrace()
            }
        }
    } else {
        val directory = File( "${Environment.getExternalStorageDirectory()}/$appName")
        if (!directory.exists()) {
            directory.mkdirs()
        }
        val fileName = System.currentTimeMillis().toString() + ".png"
        val file = File(directory, fileName)
        try {
            saveImageToStream(bitmap, FileOutputStream(file))
            val values = ContentValues()
            values.put(MediaStore.Images.Media.DATA, file.absolutePath)
            cr.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)
            isSaved = true
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }
    }

    return isSaved
}

private fun contentValues(): ContentValues {
    val values = ContentValues()
    values.put(MediaStore.Images.Media.MIME_TYPE, "image/png")
    values.put(MediaStore.Images.Media.DATE_ADDED, System.currentTimeMillis() / 1000)
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
        values.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis())
    }
    return values
}

private fun saveImageToStream(bitmap: Bitmap, outputStream: OutputStream?) {
    if (outputStream != null) {
        try {
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream)
            outputStream.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}