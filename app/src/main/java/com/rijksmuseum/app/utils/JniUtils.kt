package com.rijksmuseum.app.utils

import android.util.Base64

object JniUtils {
    init {
        System.loadLibrary("keys")
    }
    private external fun getApiKeyEncoded(): String

    fun getApiKeyDecoded() = String(Base64.decode(getApiKeyEncoded(), Base64.DEFAULT))
}