package com.rijksmuseum.app.utils

import android.content.Context
import android.view.View
import android.widget.Toast

inline fun View?.visible() {
    this?.visibility = View.VISIBLE
}

inline fun View?.gone() {
    this?.visibility = View.GONE
}

inline fun toast(context: Context, text: String, length: Int = Toast.LENGTH_LONG) {
    Toast.makeText(context, text, length).show()
}