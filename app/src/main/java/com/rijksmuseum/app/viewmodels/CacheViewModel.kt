package com.rijksmuseum.app.viewmodels

import androidx.lifecycle.ViewModel
import com.rijksmuseum.app.data.entity.ArtObject
import com.rijksmuseum.app.data.repository.ArtCacheRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class CacheViewModel @Inject constructor(
    private val mArtCacheRepo: ArtCacheRepository
) : ViewModel() {

    suspend fun getArtObjectById(artId: String): ArtObject? {
        return mArtCacheRepo.getArtById(artId)?.toDomain()
    }
}