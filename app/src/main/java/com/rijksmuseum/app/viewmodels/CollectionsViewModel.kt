package com.rijksmuseum.app.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rijksmuseum.app.data.api.server.RijksRequest
import com.rijksmuseum.app.data.entity.ArtObject
import com.rijksmuseum.app.data.entity.ArtObjectList
import com.rijksmuseum.app.data.entity.RijksList
import com.rijksmuseum.app.data.repository.ArtCacheRepository
import com.rijksmuseum.app.data.repository.RijksRepository
import com.rijksmuseum.app.utils.MAX_CACHE_RETRIEVAL
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class CollectionsViewModel @Inject constructor(
        private val mRijksRepo: RijksRepository,
        private val mArtCacheRepo: ArtCacheRepository
) : ViewModel() {
    private val TAG = "colelctionsVMtag"

    val mCollectionsChannel = Channel<RijksRequest<ArtObjectList>>()

    /**
     * Posts cached items first, meanwhile loads the form online
     */
    fun startCollectionChannel() {
        viewModelScope.launch(Dispatchers.IO) {
            val size = postCachedToChannel()
            // do not go to API if our cache is enough
            if (size > MAX_CACHE_RETRIEVAL) return@launch

            val collectionLD = mRijksRepo.fetchNextPageCollections()
            withContext(Dispatchers.Main) {
                // observing in main only
                collectionLD.observeForever { observeFetched(it) }
            }
        }
    }

    private fun observeFetched(rijksRequest: RijksRequest<RijksList>) {
        viewModelScope.launch(Dispatchers.IO) {
            val mappedData = (rijksRequest as? RijksRequest.SUCCESS)?.data?.toDomainList()
            mCollectionsChannel.send(rijksRequest.toAnother(mappedData))

            // get cached if online is failed, save to DB if success
            when (rijksRequest) {
                is RijksRequest.FAILURE -> postCachedToChannel()
                is RijksRequest.SUCCESS -> mArtCacheRepo.addToCachedCollections(rijksRequest.data)
            }
        }
    }

    /**
     * Send cached objects to channel and
     * @return size
     */
    private suspend fun postCachedToChannel(): Int {
        val cachedArtObjects = mArtCacheRepo.getCachedCollections()
        val rl = ArtObjectList(cachedArtObjects.map { it.toDomain() }, isCached = true)
        mCollectionsChannel.send(RijksRequest.SUCCESS(rl))
        return rl.artObjectList.size
    }

    fun loadNextPage() {
        viewModelScope.launch(Dispatchers.IO) {
            val collectionLD = mRijksRepo.fetchNextPageCollections()
            withContext(Dispatchers.Main) { observeCollections(collectionLD) }
        }
    }

    private fun observeCollections(collectionLD: LiveData<RijksRequest<RijksList>>) {
        collectionLD.observeForever { request ->
            val mappedData = (request as? RijksRequest.SUCCESS)?.data?.toDomainList()
            viewModelScope.launch { mCollectionsChannel.send(request.toAnother(mappedData)) }
            if (request is RijksRequest.SUCCESS) {
                viewModelScope.launch(Dispatchers.IO) {
                    mArtCacheRepo.addToCachedCollections(request.data)
                }
            }
        }
    }
}