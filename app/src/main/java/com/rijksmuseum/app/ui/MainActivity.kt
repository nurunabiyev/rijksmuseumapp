package com.rijksmuseum.app.ui

import android.content.pm.PackageManager
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.rijksmuseum.app.R
import com.rijksmuseum.app.ui.master.CollectionsFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.launch

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    companion object {
        const val REQUEST = 112
        private const val TAG = "mainactivitylog"
    }

    private var mCollectionsFragment: CollectionsFragment? = null
    val mPermissionGranted = Channel<Boolean>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mCollectionsFragment = if (savedInstanceState != null) {
            supportFragmentManager.getFragment(
                    savedInstanceState,
                    CollectionsFragment.TAG,
            ) as CollectionsFragment
        } else {
            CollectionsFragment()
        }

        lifecycleScope.launchWhenResumed {
            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.fragment_container_fl, mCollectionsFragment!!)
                    .commit()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        mCollectionsFragment?.let {
            supportFragmentManager.putFragment(outState, CollectionsFragment.TAG, it)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<out String>,
                                            grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST -> {
                lifecycleScope.launch {
                    val granted = grantResults.isNotEmpty()
                            && grantResults[0] === PackageManager.PERMISSION_GRANTED
                    mPermissionGranted.send(granted)
                }
                return
            }
        }

    }
}