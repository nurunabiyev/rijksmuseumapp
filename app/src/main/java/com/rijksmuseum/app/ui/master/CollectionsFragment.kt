package com.rijksmuseum.app.ui.master

import android.os.Bundle
import android.os.Parcelable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.transition.Transition
import androidx.transition.TransitionManager
import com.google.android.material.transition.MaterialContainerTransform
import com.google.android.material.transition.MaterialSharedAxis
import com.rijksmuseum.app.viewmodels.CollectionsViewModel
import com.rijksmuseum.app.R
import com.rijksmuseum.app.data.api.server.RijksRequest
import com.rijksmuseum.app.data.entity.ArtObject
import com.rijksmuseum.app.data.entity.ArtObjectList
import com.rijksmuseum.app.ui.detail.ArtDetailFragment
import com.rijksmuseum.app.ui.master.ArtsAdapter.ArtsAdapterListener
import com.rijksmuseum.app.utils.gone
import com.rijksmuseum.app.utils.visible
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_collections.view.*


/**
 * A Fragment that hosts a toolbar and a child fragment with a list of art data.
 */
@AndroidEntryPoint
class CollectionsFragment : Fragment(), ArtsAdapterListener {

    private val mCollectionsVM: CollectionsViewModel by viewModels()

    private lateinit var mView: View
    private var isErrorShown = false
    private var listState: Parcelable? = null
    private var mArtObjects = ArrayList<ArtObject>()
    private var mAdapter: ArtsAdapter? = null

    override fun onCreateView(
            layoutInflater: LayoutInflater,
            viewGroup: ViewGroup?,
            bundle: Bundle?
    ): View {
        mView = layoutInflater.inflate(R.layout.fragment_collections, viewGroup, false)
        return mView
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onViewCreated(view: View, bundle: Bundle?) {
        val channelIterator = mCollectionsVM.mCollectionsChannel.iterator()

        lifecycleScope.launchWhenResumed {
            if (mArtObjects.isNotEmpty()) {
                val sharedAxis = MaterialSharedAxis(MaterialSharedAxis.Z, true)
                setList(sharedAxis)
            } else {
                mCollectionsVM.startCollectionChannel()
            }
            while (channelIterator.hasNext()) {
                val listRequest = channelIterator.next()
                collectionsFetched(listRequest)
            }
        }
    }

    private fun collectionsFetched(rijksRequest: RijksRequest<ArtObjectList>) {
        when (rijksRequest) {
            is RijksRequest.LOADING -> mView.collections_loader_pb?.visible()
            is RijksRequest.SUCCESS -> fetchedSuccess(rijksRequest.data)
            is RijksRequest.FAILURE -> {
                mView.collections_loader_pb?.gone()
                mView.error_loaded_tv?.text = "Error: ${rijksRequest.text}"
                mView.error_loaded_tv?.isSelected = true
                mView.error_loaded_tv?.visible()
                isErrorShown = true
            }
        }
    }

    /**
     * Received either cached or from API
     */
    private fun fetchedSuccess(data: ArtObjectList?) {
        when (data?.isCached) {
            true -> showCachedList(data)
            else -> showApiList(data)
        }

        if (isErrorShown) mView.error_loaded_tv.visible() else mView.error_loaded_tv.gone()
    }

    private fun showCachedList(data: ArtObjectList) {
        // from cache we receive everything
        mArtObjects = (data.artObjectList) as ArrayList<ArtObject>
        if (!isErrorShown) mView.collections_loader_pb.visible() else mView.collections_loader_pb.gone()

        val sharedAxis = MaterialSharedAxis(MaterialSharedAxis.Z, true)
        setList(sharedAxis)
    }

    private fun showApiList(data: ArtObjectList?) {
        val wasListEmpty = mArtObjects.isEmpty()
        // from online we receive last 10 only
        mArtObjects.addAll(data?.artObjectList ?: emptyList())
        mView.collections_loader_pb?.gone()
        isErrorShown = false

        if (wasListEmpty) {
            val sharedAxis = MaterialSharedAxis(MaterialSharedAxis.Z, true)
            setList(sharedAxis)
        } else {
            mAdapter?.submitList(mArtObjects)
        }
    }

    override fun onDestroyView() {
        val rv: RecyclerView? = requireView().findViewById(ARTS_RECYCLER_VIEW_ID)
        if (rv != null) {
            Log.d(TAG, "onDestroyView: destroyed rv")
            listState = rv.layoutManager!!.onSaveInstanceState()
        }
        super.onDestroyView()
    }

    override fun onArtClicked(view: View?, artObject: ArtObject?) {
        if (artObject?.id == null) return

        val fragment = ArtDetailFragment.newInstance(artObject.id!!)
        val transform = MaterialContainerTransform()
        fragment.sharedElementEnterTransition = transform
        parentFragmentManager.beginTransaction()
                .replace(R.id.fragment_container_fl, fragment)
                .addToBackStack(ArtDetailFragment.TAG)
                .commit()
    }

    /**
     * Add or replace the RecyclerView containing the list of art objects with a new RecyclerView that is
     * either a list/grid and sorted/unsorted according to the given arguments.
     */
    private fun setList(transition: Transition) {
        // Use a Transition to animate the removal and addition of the RecyclerViews.
        val recyclerView = createRecyclerView()
        // Restore the RecyclerView's scroll position if available
        if (listState != null) {
            recyclerView.layoutManager!!.onRestoreInstanceState(listState)
            listState = null
        }
        transition.addTarget(recyclerView)
        val currentRecyclerView = mView.list_container_fl!!.getChildAt(0)
        if (currentRecyclerView != null) {
            transition.addTarget(currentRecyclerView)
        }
        TransitionManager.beginDelayedTransition(mView.list_container_fl!!, transition)
        mAdapter = ArtsAdapter(this, lifecycleScope)
        recyclerView.adapter = mAdapter
        if (mArtObjects.isEmpty()) mView.no_collections_tv?.visible() else mView.no_collections_tv?.gone()
        mAdapter?.submitList(mArtObjects)
        mView.collections_loader_pb?.gone()
        Log.d(TAG, "setList: created list with ${mArtObjects.size} items")
        mView.list_container_fl!!.removeAllViews()
        mView.list_container_fl!!.addView(recyclerView)
    }

    private fun createRecyclerView(): RecyclerView {
        val context = requireContext()
        return RecyclerView(context).apply {
            id = ARTS_RECYCLER_VIEW_ID
            layoutParams = FrameLayout.LayoutParams(
                    FrameLayout.LayoutParams.MATCH_PARENT,
                    FrameLayout.LayoutParams.MATCH_PARENT
            )
            val verticalPadding =
                    context.resources.getDimensionPixelSize(R.dimen.list_padding_vertical)
            setPadding(0, verticalPadding, 0, verticalPadding)
            clipToPadding = false
            layoutManager = GridLayoutManager(context, GRID_SPAN_COUNT)
            addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                    super.onScrollStateChanged(recyclerView, newState)
                    if (!recyclerView.canScrollVertically(1)) {
                        Log.d(TAG, "onScrollStateChanged: fetching next page")
                        mView.collections_loader_pb?.visible()
                        mCollectionsVM.loadNextPage()
                    }
                }
            })
        }
    }

    companion object {
        internal const val TAG = "collectionsfragmenttag"
        private const val GRID_SPAN_COUNT = 2
        private val ARTS_RECYCLER_VIEW_ID = ViewCompat.generateViewId()
    }
}