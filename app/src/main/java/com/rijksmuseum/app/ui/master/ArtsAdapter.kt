package com.rijksmuseum.app.ui.master

import android.graphics.drawable.Drawable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.lifecycle.LifecycleCoroutineScope
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.rijksmuseum.app.R
import com.rijksmuseum.app.data.entity.ArtObject
import com.rijksmuseum.app.ui.master.ArtsAdapter.ArtViewHolder
import com.rijksmuseum.app.utils.gone
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * An adapter which displays a list of art objects in a grid layout.
 */
internal class ArtsAdapter(
    private val listener: ArtsAdapterListener,
    val lifecycleScope: LifecycleCoroutineScope
) :
    ListAdapter<ArtObject, ArtViewHolder>(ArtObject.DIFF_CALLBACK) {

    interface ArtsAdapterListener {
        fun onArtClicked(view: View?, artObject: ArtObject?)
    }

    private val itemLayout: Int = R.layout.list_item_photo

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ArtViewHolder {
        val li = LayoutInflater.from(viewGroup.context).inflate(itemLayout, viewGroup, false)
        return ArtViewHolder(li, listener)
    }

    override fun onBindViewHolder(artViewHolder: ArtViewHolder, i: Int) {
        artViewHolder.bind(getItem(i))
    }

    inner class ArtViewHolder(view: View, private val listener: ArtsAdapterListener) :
        RecyclerView.ViewHolder(view) {
        private val container: View = view.findViewById(R.id.art_item_container)
        private val artImage: ImageView = view.findViewById(R.id.art_header_iv)
        private val artTitle: TextView = view.findViewById(R.id.art_title_tv)
        private val artInfo: TextView = view.findViewById(R.id.art_info_tv)
        private val artLoader: ProgressBar = view.findViewById(R.id.item_loader_pb)

        fun bind(artObject: ArtObject) {
            ViewCompat.setTransitionName(container, artObject.title)
            container.setOnClickListener { listener.onArtClicked(container, artObject) }
            lifecycleScope.launch { setImage(artObject) }
            artTitle.text = artObject.title
            artInfo.text = artObject.longTitle
            artTitle.isSelected = true
        }

        private fun setImage(artObject: ArtObject) {
            val errorDrawable = ContextCompat.getDrawable(container.context, R.drawable.ic_error)
            Glide.with(container)
                .load(artObject.headerImageUrl)
                .addListener(object : RequestListener<Drawable?> {
                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any?,
                        target: Target<Drawable?>?,
                        isFirstResource: Boolean
                    ): Boolean {
                        lifecycleScope.launch(Dispatchers.Main) {
                            artLoader.gone()
                            artImage.setImageDrawable(errorDrawable)
                        }
                        return true
                    }

                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any?,
                        target: Target<Drawable?>?,
                        dataSource: DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {
                        lifecycleScope.launch(Dispatchers.Main) {
                            artLoader.gone()
                            artImage.setImageDrawable(resource ?: errorDrawable)
                        }
                        return true
                    }
                }).submit()
        }
    }

}