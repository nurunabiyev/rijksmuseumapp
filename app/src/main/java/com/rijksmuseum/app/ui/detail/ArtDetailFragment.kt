package com.rijksmuseum.app.ui.detail

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.rijksmuseum.app.R
import com.rijksmuseum.app.data.entity.ArtObject
import com.rijksmuseum.app.ui.MainActivity
import com.rijksmuseum.app.ui.MainActivity.Companion.REQUEST
import com.rijksmuseum.app.utils.*
import com.rijksmuseum.app.viewmodels.CacheViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_detail.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

/**
 * A Fragment that displays an art object's details.
 */
@AndroidEntryPoint
class ArtDetailFragment : Fragment() {
    private lateinit var mView: View

    private val mCacheVM: CacheViewModel by viewModels()
    private var mDrawable: Drawable? = null
    private var mArtObject: ArtObject? = null

    override fun onCreateView(
            layoutInflater: LayoutInflater,
            viewGroup: ViewGroup?,
            bundle: Bundle?
    ): View {
        mView = layoutInflater.inflate(R.layout.fragment_detail, viewGroup, false)
        return mView
    }

    override fun onViewCreated(view: View, bundle: Bundle?) {
        val artId = requireArguments().getString(ART_ID_KEY, null)
        mCacheVM // init view model before using
        lifecycleScope.launch(Dispatchers.IO) {
            mArtObject = mCacheVM.getArtObjectById(artId)
            withContext(Dispatchers.Main) { setArtObjectUi(mArtObject) }
        }
        initMenuClickListener()
        initPermissionListener()
    }

    private fun initPermissionListener() {
        lifecycleScope.launch {
            (activity as MainActivity).mPermissionGranted.receiveAsFlow().collectLatest {
                if (it) {
                    saveToGallery()
                } else {
                    toast(requireContext(), getString(R.string.need_permission))
                }
            }
        }

    }

    private fun setArtObjectUi(artObject: ArtObject?) {
        if (artObject == null) {
            toast(requireContext(), getString(R.string.error_art_not_found))
            return
        }

        val toolbar: Toolbar = mView.toolbar
        val artTitle = mView.art_title_tv
        val artInfo = mView.art_info_tv

        ViewCompat.setTransitionName(mView.container_ll, artObject.title)
        ViewCompat.setElevation(toolbar, 0f)
        toolbar.setNavigationOnClickListener { requireActivity().onBackPressed() }

        lifecycleScope.launch { setPhoto(artObject) }
        artTitle.text = artObject.title
        artInfo.text = artObject.longTitle
        artTitle.isSelected = true
    }

    private fun setPhoto(artObject: ArtObject) {
        val errorDrawable = ContextCompat.getDrawable(requireContext(), R.drawable.ic_error)
        val artImage = mView.art_header_iv
        val progressBarDetail = mView.detail_image_loader_pb

        Glide.with(mView.container_ll)
                .load(artObject.webImageUrl)
                .addListener(object : RequestListener<Drawable?> {
                    override fun onLoadFailed(
                            e: GlideException?,
                            model: Any?,
                            target: Target<Drawable?>?,
                            isFirstResource: Boolean
                    ): Boolean {
                        lifecycleScope.launch(Dispatchers.Main) {
                            progressBarDetail.gone()
                            artImage.setImageDrawable(errorDrawable)
                        }
                        return true
                    }

                    override fun onResourceReady(
                            resource: Drawable?,
                            model: Any?,
                            target: Target<Drawable?>?,
                            dataSource: DataSource?,
                            isFirstResource: Boolean
                    ): Boolean {
                        lifecycleScope.launch(Dispatchers.Main) {
                            progressBarDetail.gone()
                            mDrawable = resource
                            artImage.setImageDrawable(resource ?: errorDrawable)
                        }
                        return true
                    }
                }).submit()
    }

    private fun initMenuClickListener() {
        mView.toolbar.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.item_share -> shareImageLink()
                R.id.item_save -> saveToGallery()
            }
            true
        }
    }

    private fun shareImageLink() {
        val intent = Intent(Intent.ACTION_SEND).apply {
            type = "text/plain"
            val sharedText = String.format(getString(R.string.shared_via), mArtObject?.title ?: "N/A")
            putExtra(Intent.EXTRA_SUBJECT, sharedText)
            putExtra(Intent.EXTRA_TEXT, mArtObject?.webImageUrl)
        }
        startActivity(Intent.createChooser(intent, getString(R.string.share_using)))
    }

    private fun saveToGallery() {
        if (mDrawable == null) {
            toast(requireContext(), getString(R.string.please_retry))
            return
        }

        val bitmap = (mDrawable as BitmapDrawable).bitmap

        if (Build.VERSION.SDK_INT >= 23) {
            val permissions = arrayOf(
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE)

            if (!hasPermissions(context, permissions)) {
                ActivityCompat.requestPermissions(requireActivity(), permissions, REQUEST)
                return
            }
        }

        lifecycleScope.launch(Dispatchers.IO) {
            val isSaved = saveImageToGallery(bitmap, requireActivity(), getString(R.string.app_name))
            if (isSaved) {
                withContext(Dispatchers.Main) { toast(requireContext(), getString(R.string.saved)) }
            }
        }
    }


    private fun hasPermissions(context: Context?, permissions: Array<String>): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null) {
            for (permission in permissions) {
                val currPermission = ActivityCompat.checkSelfPermission(context, permission)
                if (currPermission != PackageManager.PERMISSION_GRANTED) {
                    return false
                }
            }
        }
        return true
    }

    companion object {
        const val TAG = "artdetailfragment"
        private const val ART_ID_KEY = "art_id_key"
        fun newInstance(artId: String): ArtDetailFragment {
            val fragment = ArtDetailFragment()
            val bundle = Bundle()
            bundle.putString(ART_ID_KEY, artId)
            fragment.arguments = bundle
            return fragment
        }
    }
}