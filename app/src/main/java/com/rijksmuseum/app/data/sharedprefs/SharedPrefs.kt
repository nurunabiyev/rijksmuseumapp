package com.rijksmuseum.app.data.sharedprefs

import android.content.Context
import android.content.SharedPreferences
import com.rijksmuseum.app.RmApp

/**
 * Abstract class, all preferences should be subclass of this class
 *  to easily retrieve preferences
 */
abstract class SharedPrefs {

    companion object {
        private const val RIJKS_PREFS = "rikjsmuseumpreferences"
    }

    private val app = RmApp.get()

    fun getPrefs(): SharedPreferences? = app.getSharedPreferences(RIJKS_PREFS, Context.MODE_PRIVATE)

    fun editPrefs(): SharedPreferences.Editor? = getPrefs()?.edit()

    fun clearPreferences() = editPrefs()?.clear()?.apply()
}