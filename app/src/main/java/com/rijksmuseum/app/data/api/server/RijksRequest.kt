package com.rijksmuseum.app.data.api.server

/**
 * This class is used for any network request that returns any arbitrary data
 * Used as observable for reactive kotlin
 */
sealed class RijksRequest<out T> {
    data class SUCCESS<T>(var data: T? = null) : RijksRequest<T>()
    data class FAILURE(var text: String) : RijksRequest<Nothing>()
    object LOADING : RijksRequest<Nothing>()

    fun <T> toAnother(data: T?): RijksRequest<T> {
        return when (this) {
            is SUCCESS -> SUCCESS(data)
            is FAILURE -> FAILURE(text)
            is LOADING -> LOADING
        }
    }
}