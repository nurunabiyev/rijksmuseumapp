package com.rijksmuseum.app.data.entity

import androidx.room.*

data class RijksList(
    var artObjects: List<ArtObjectDto>?,
    var count: Int? = 0,
    var countFacets: ArtObjectDto.CountFacets? = null,
    var elapsedMilliseconds: Int? = null,
    var facets: List<ArtObjectDto.Facet>? = null,
    var isCached: Boolean = false
) {
    fun toDomainList(): ArtObjectList {
        return ArtObjectList(
            artObjectList = artObjects?.map { it.toDomain() } ?: emptyList(),
            isCached = isCached
        )
    }
}

@Entity(tableName = "artObjects")
data class ArtObjectDto(
    @PrimaryKey(autoGenerate = true)
    var _rowId: Int
) {
    var hasImage: Boolean? = null

    @Embedded(prefix = "headerImage")
    var headerImage: HeaderImage? = null
    var id: String? = null

    @Ignore
    var links: Links? = null
    var longTitle: String? = null
    var objectNumber: String? = null
    var permitDownload: Boolean? = null
    var principalOrFirstMaker: String? = null

    @Ignore
    var productionPlaces: List<String>? = null
    var showImage: Boolean? = null
    var title: String? = null

    @Embedded(prefix = "webImage")
    var webImage: WebImage? = null

    fun toDomain(): ArtObject {
        return ArtObject(
            title = title ?: "N/A",
            longTitle = longTitle ?: "N/A",
            headerImageUrl = headerImage?.url,
            webImageUrl = webImage?.url,
            id = id
        )
    }

    data class CountFacets(
        var hasimage: Int?,
        var ondisplay: Int?
    )

    data class Facet(
        var facets: List<FacetX>?,
        var name: String?,
        var otherTerms: Int?,
        var prettyName: Int?
    )

    data class HeaderImage(
        var guid: String?,
        var height: Int?,
        var offsetPercentageX: Int?,
        var offsetPercentageY: Int?,
        var url: String?,
        var width: Int?
    )

    data class Links(
        var self: String?,
        var web: String?
    )

    data class WebImage(
        var guid: String?,
        var height: Int?,
        var offsetPercentageX: Int?,
        var offsetPercentageY: Int?,
        var url: String?,
        var width: Int?
    )

    data class FacetX(
        var key: String?,
        var value: Int?
    )
}