package com.rijksmuseum.app.data.api.db.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.rijksmuseum.app.data.entity.ArtObjectDto

@Dao
interface ArtObjectsDao {
    @Query("SELECT * FROM artObjects")
    suspend fun getAllArtObjects(): List<ArtObjectDto>

    @Query("SELECT * FROM artObjects where id=:id LIMIT 1")
    suspend fun getArtObjectById(id: String): ArtObjectDto?

    @Insert
    suspend fun insert(vararg artObjectDto: ArtObjectDto)

    @Delete
    suspend fun delete(vararg artObjectDto: ArtObjectDto)

    @Query("DELETE FROM artObjects")
    suspend fun deleteAll()
}