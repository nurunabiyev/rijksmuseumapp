package com.rijksmuseum.app.data.repository

import com.rijksmuseum.app.data.api.db.dao.ArtObjectsDao
import com.rijksmuseum.app.data.entity.ArtObjectDto
import com.rijksmuseum.app.data.entity.RijksList
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ArtCacheRepository @Inject constructor(
    private val artObjectsDao: ArtObjectsDao
) {
    suspend fun addToCachedCollections(collections: RijksList?) {
        collections?.artObjects?.forEach { artObjectsDao.insert(it) }
    }

    suspend fun getCachedCollections(): List<ArtObjectDto> {
        return artObjectsDao.getAllArtObjects()
    }

    suspend fun getArtById(artId: String): ArtObjectDto? {
        return artObjectsDao.getArtObjectById(artId)
    }
}