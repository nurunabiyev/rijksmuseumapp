package com.rijksmuseum.app.data.api.db

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.rijksmuseum.app.data.entity.ArtObjectDto

/**
 * Generic converter that helps to retrieve objects for room db
 * Because [fromStringToObj] does not retain object type,
 *  we need to extend it to any class that we want to store/retrieve in db
 */
abstract class CoreConverter<T> {
    @TypeConverter
    fun fromStringToList(value: String): List<T>? {
        val listType = object : TypeToken<List<T>>() {}.type
        return Gson().fromJson<List<T>>(value, listType)
    }

    @TypeConverter
    abstract fun fromStringToObj(value: String): T?
}

class ArtObjectConverter : CoreConverter<ArtObjectDto>() {
    @TypeConverter
    override fun fromStringToObj(value: String): ArtObjectDto? {
        val type = object : TypeToken<ArtObjectDto>() {}.type
        return Gson().fromJson<ArtObjectDto>(value, type)
    }
}