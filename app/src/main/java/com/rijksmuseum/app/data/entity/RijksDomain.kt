package com.rijksmuseum.app.data.entity

import androidx.recyclerview.widget.DiffUtil

data class ArtObjectList(
    var artObjectList: List<ArtObject>,
    var isCached: Boolean = false
)

/**
 * Domain object used though UI
 */
data class ArtObject(
    var title: String,
    var longTitle: String,
    val headerImageUrl: String? = null,
    val webImageUrl: String? = null,
    var id: String?
) {

    companion object {
        @JvmField
        val DIFF_CALLBACK: DiffUtil.ItemCallback<ArtObject> =
            object : DiffUtil.ItemCallback<ArtObject>() {
                override fun areItemsTheSame(
                    newItem: ArtObject,
                    oldItem: ArtObject
                ): Boolean {
                    return newItem.id == oldItem.id
                }

                override fun areContentsTheSame(
                    newItem: ArtObject,
                    oldItem: ArtObject
                ): Boolean {
                    return false
                }
            }
    }
}