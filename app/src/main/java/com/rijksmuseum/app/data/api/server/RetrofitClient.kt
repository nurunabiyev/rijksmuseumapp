package com.rijksmuseum.app.data.api.server

import android.content.Context
import com.google.gson.GsonBuilder
import com.google.gson.JsonDeserializer
import com.rijksmuseum.app.BuildConfig
import okhttp3.ConnectionPool
import okhttp3.Dispatcher
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.net.CookieManager
import java.net.CookiePolicy
import java.util.*
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

object RetrofitClient {
    private var retrofit: Retrofit? = null

    fun getClient(baseUrl: String): Retrofit {
        if (retrofit != null) {
            return retrofit!!
        }

        val interceptor = HttpLoggingInterceptor()
        interceptor.level =
            if (BuildConfig.BUILD_TYPE === "debug") HttpLoggingInterceptor.Level.BODY
            else HttpLoggingInterceptor.Level.NONE

        val cookieHandler = CookieManager()
        cookieHandler.setCookiePolicy(CookiePolicy.ACCEPT_ALL)

        val dispatcher = Dispatcher(Executors.newFixedThreadPool(10))
        dispatcher.maxRequests = 20
        dispatcher.maxRequestsPerHost = 20

        val client = OkHttpClient.Builder()
            .addNetworkInterceptor(interceptor)
            .connectTimeout(10, TimeUnit.SECONDS)
            .writeTimeout(10, TimeUnit.SECONDS)
            .readTimeout(10, TimeUnit.SECONDS)
            .dispatcher(dispatcher)
            .connectionPool(ConnectionPool(100, 30, TimeUnit.SECONDS))
            .build()

        val jsonDeserializer = JsonDeserializer { json, _, _ -> Date(json.asJsonPrimitive.asLong) }
        val gson = GsonBuilder()
            .setLenient()
            .serializeNulls()
            .registerTypeAdapter(Date::class.java, jsonDeserializer)
            .create()

        retrofit = Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(client)
            .build()

        return retrofit as Retrofit
    }
}