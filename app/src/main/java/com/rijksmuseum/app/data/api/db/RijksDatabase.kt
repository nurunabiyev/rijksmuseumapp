package com.rijksmuseum.app.data.api.db

import android.content.Context
import androidx.room.*
import com.rijksmuseum.app.BuildConfig
import com.rijksmuseum.app.data.api.db.dao.ArtObjectsDao
import com.rijksmuseum.app.data.entity.ArtObjectDto


@Database(
    entities = [ArtObjectDto::class],
    version = 1,
    exportSchema = false
)
@TypeConverters(ArtObjectConverter::class)
abstract class RijksDatabase : RoomDatabase() {
    abstract val artObjectsDao: ArtObjectsDao

    companion object {
        private const val DB_NAME = "rijksmuseum-${BuildConfig.BUILD_TYPE}.db"

        @Volatile
        private var INSTANCE: RijksDatabase? = null

        fun getInstance(context: Context): RijksDatabase {
            return INSTANCE ?: synchronized(this) {
                INSTANCE ?: create(context).also { INSTANCE = it }
            }
        }

        private fun create(context: Context): RijksDatabase {
            return Room
                .databaseBuilder(context.applicationContext, RijksDatabase::class.java, DB_NAME)
                .fallbackToDestructiveMigration()
                .build()
        }
    }
}