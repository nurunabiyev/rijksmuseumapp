package com.rijksmuseum.app.data.sharedprefs


/**
 * Preferences of page are edited/retrieved here
 */
object PageCachePrefs : SharedPrefs() {
    private const val PREFS_CURR_PAGE_NUM = "current_page_num"

    fun incrementPage() {
        val incrementTo = getCurrentPage() + 1
        editPrefs()?.putInt(PREFS_CURR_PAGE_NUM, incrementTo)?.apply()
    }

    fun getCurrentPage(): Int {
        return getPrefs()?.getInt(PREFS_CURR_PAGE_NUM, 1) ?: 1
    }

}