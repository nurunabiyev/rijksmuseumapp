package com.rijksmuseum.app.data.api.server

import com.rijksmuseum.app.data.entity.RijksList
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * APIs endpoints for Rijksmuseum https://data.rijksmuseum.nl/object-metadata/api/
 */
interface RijksmuseumService {

    companion object {
        // TODO ideally language should be set in the settings
        private const val BASE_API_URL = "https://www.rijksmuseum.nl/api/en/"

        fun create(): RijksmuseumService {
            val retrofit = RetrofitClient.getClient(BASE_API_URL)
            return retrofit.create(RijksmuseumService::class.java)
        }
    }

    @GET("collection")
    fun getAllCollections(
        @Query("key") key: String,
        @Query("p") page: Int
    ): Call<RijksList>

}
