package com.rijksmuseum.app.data.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.rijksmuseum.app.data.api.server.RijksRequest
import com.rijksmuseum.app.data.api.server.RijksmuseumService
import com.rijksmuseum.app.data.entity.RijksList
import com.rijksmuseum.app.data.sharedprefs.PageCachePrefs
import com.rijksmuseum.app.utils.JniUtils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RijksRepository @Inject constructor(private val mRijksService: RijksmuseumService) {
    private val TAG = "rijksrepologtag"

    /**
     * Fetches a required page from API
     * @return live data that holds the page
     */
    fun fetchNextPageCollections(): LiveData<RijksRequest<RijksList>> {
        val liveData = MutableLiveData<RijksRequest<RijksList>>()
        liveData.postValue(RijksRequest.LOADING)

        val key = JniUtils.getApiKeyDecoded()
        val page = PageCachePrefs.getCurrentPage()
        val call = mRijksService.getAllCollections(key, page)
        call.enqueue(object : Callback<RijksList> {
            override fun onResponse(call: Call<RijksList>, response: Response<RijksList>) {
                Log.d(TAG, "getOnlineCollections received: " +
                        " ${call.request().url()}" +
                        " ${response.code()}" +
                        " ${response.body()}")
//                Log.d(TAG, "getOnlineCollections received: ${response.request().url()}")

                when (response.code()) {
                    200 -> {
                        PageCachePrefs.incrementPage()
                        liveData.postValue(RijksRequest.SUCCESS(response.body()))
                    }
                    else -> liveData.postValue(RijksRequest.FAILURE("Error fetching collections"))
                }
            }

            override fun onFailure(call: Call<RijksList>, t: Throwable) {
                val log = "onFailure getOnlineCollections:" +
                        " ${t.message}: ${t.stackTraceToString()}"
                Log.d(TAG, log)
                liveData.postValue(RijksRequest.FAILURE("Error fetching collections"))
            }

        })

        return liveData
    }

}