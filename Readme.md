# RijksMuseum Android App
This is a sample android app with simple Master-Detail concept written in Kotlin. It showcases various photos from official [Rijksmuseum website](https://data.rijksmuseum.nl/object-metadata/api/).

## Installation and Build
You can install APK directly from `/app/release/app-release.apk`. In order to build from scratch, you will need to install Android SDK and NDK and use Gradle (or from Android Studio Directly).

## Tech-Stack

**Libraries used:**

* Android Jetpack
* Room for database
* Hilt for Dependency Injection
* Coroutines e.g. Flow and Lifecycle scope used
* Glide to fetch and display photos
* Retrofit/Okhttp3 for networking
* NDK to securely (somewhat) store API key

**Architecture concepts applied:**

* MVVM
* SOLID principles
* Clean Code principles
* Part of DDD concepts applied, such as dividing Domain and DTO objects


